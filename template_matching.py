#!/usr/bin/env python
import numpy as np
import skimage,skimage.io #, scipy.ndimage, scipy.interpolate, scipy.signal
import gym # not need for the first part

#get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
import matplotlib.pyplot as plt
from skimage import feature
from skimage.transform import resize

def template_matching(img,img1):
    x=skimage.feature.match_template(img,img1)
    y=np.max(x)
    index=np.where(x==y)
    return index[0],index[1]

def find_similar_part(template,mask):    
    img=skimage.color.rgb2gray(template)
    #minsize=min(template.shape[0],template.shape[1])
    if(mask.shape[0]>img.shape[0] or mask.shape[1]>img.shape[1]):
        mask=resize(mask,(img.shape[0]//5,img.shape[1]//5))
        
    img_mask=skimage.color.rgb2gray(mask)
    

    y,x=template_matching(img,img_mask)
    fig = plt.figure()
    ax1 = plt.subplot(1,1,1)point
    ax1.imshow(img_mask, cmap=plt.cm.gray)
    plt.title("what to detect")


    fig = plt.figure()
    ax1 = plt.subplot(1,1,1)
    ax1.imshow(img, cmap=plt.cm.gray)
    hh, ww = img_mask.shape
    rect = plt.Rectangle((x, y), ww, hh, edgecolor='r', facecolor='none')
    ax1.add_patch(rect)
    plt.title("Detected")
    plt.show()

img1=skimage.io.imread("resources/kodim15.png")
img_mask1=skimage.io.imread("resources/kodim_mask.png")
find_similar_part(img1,img_mask1)

img=skimage.io.imread("pacman.jpg")
#img_mask=skimage.io.imread("mask.png")#("pac_mask1.png")
img_mask=img[800:1100,100:500,:]
find_similar_part(img,img_mask)

