Development of a movie recommendation system. <br/>
Use of machine learning algorithms for classification. <br/>
Use of genetic algorithms to solve - approximate functions.<br/>
All code is written in python with the use of Jupyter notebooks.
Developed by [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous)
